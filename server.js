// библиотека для работы с путями
var path = require('path')
// подключаем веб-сервер (express)
var express = require('express')
var app = express()
// экземпляр http сервера, он будет обслуживать запросы с помощью express
var http = require('http').Server(app)
// подлючаем socket.io
// вебсокет'у нужен http сервер, как транспортный уровень 
var io = require('socket.io')(http)
// подключим упаковщик, он использует babel для компиляции кода
var Bundler = require('parcel-bundler')
// создаем экземпляр упаковщика с точкой входа
var bundler = new Bundler(path.join('public', 'index.html'), {})
var ok = 'ok';
// подключим упаковщик как промежуточный слой
app.use(bundler.middleware())
// укажем папку где будут хранится статичные файлы (html, css, images)
app.use(express.static(path.join(__dirname, 'public')))

// скажем, что веб-сервер может обслуживать пути
// здесь мы говорим что, на все запросы будет высылаться index.html 
// поскольку логика работы приложения будет в javascript 
// здесь больше ничего не надо, мы не делаем RestAPI, т.к.
// мы будем использовать socket.io как транспортный слой
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'))
})

// при подключении веб-сокета, назначим события для сокета
io.on('connection', function (socket) {
  // если пришло событие типа "say" выполним следующую ф-цию
  socket.on('say', function (data, callback) {
    console.log(data)
    // если пришел запрос, сокету можно вернуть какой-то результат
    // callback передается вторым параметром
    // если не указывать callback, то сокет не будет ждать вызов callback
    // если не использовать возможности callback 
    // тогда, если надо передать данные на клиент,
    // нужно вызвать socket.emit('say', { data })
    // но тогда и клиент должен подписаться на событие socket.on('say', function(data) {...})

    // если есть callback вызвем его
    // в кач-ве параметра передадим объект
    callback && callback({ server: ok })
  })
})

// запустим сервер
http.listen(3000, () => {
  console.log('server started at http://localhost:3000')
})
//dd

///