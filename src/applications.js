import ReactDOM from 'react-dom'
import React from 'react'
import io from 'socket.io-client/lib'

// создадим экземпляр клиента socket.io
// по умолчанию сам найдет сокет-сервер
const socket = io({})

// создадим класс приложения
class Application extends React.Component {

  // в конструкторе класса создадим состояние компонента
  constructor(props) {
    super(props)
    this.state = {
      test: "test",
      server: null
    }
  }

  // после создания компонента
  componentDidMount() {
    // вызовем на сервере через сокет путь "say", 
    // и передадим в кач-ве параметра объекта
    // здесь, 3-м парамтром передадим ф-цию
    // если есть 3-й параметр, то сервер должен передать объект с результатом вызова
    socket.emit('say',  test , (response) => {
      console.log(response)
      // выстовим переменную "server" в состоянии, 
      // состояние передаст сервер
      this.setState({ server: response.server })
    })
  }

  // рисуем интерфейс
  render() {
    return <React.Fragment>
      <div>Application</div>
      <div>server: {this.state.server}</div>
    </React.Fragment>
  }
}

// вставим компонент на страницу в элемент с id='app'
ReactDOM.render(<Application />, document.getElementById('app'))